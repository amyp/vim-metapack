function! s:attach_metapacks(...)
  call s:config_for_load.add_metapacks(a:000)
endfunction
function! metapack#task#load_metapacks(config) abort
  if has_key(a:config.apply_spec, 'metapack_list')
    call a:config.add_metapacks(a:config.apply_spec.metapack_list)
  else
    let s:config_for_load = a:config
    command -nargs=+ Metapack call s:attach_metapacks(<f-args>)

    try
      execute 'source' a:config.list_file
    finally
      delcommand Metapack
      unlet s:config_for_load
    endtry
  endif

  " TODO: demand-load hook or autocmd
  let l:failed = v:false
  call metapack#hooks#begin({
        \ 'collect': { 'type': 'function', 'nargs': 0 },
        \ 'config': { 'type': 'function', 'nargs': 0 },
        \ 'init': { 'type': 'lines', 'nargs': 0 },
        \ 'generate': { 'type': 'function', 'nargs': 0 },
        \})

  for l:mp in a:config.metapack_order
    echo 'Loading '.l:mp.name
    let l:failed = metapack#hooks#read_file(l:mp.hooks, l:mp.path)
    if l:failed
      break
    endif
  endfor

  call metapack#hooks#end()
  return l:failed
endfunction

function! metapack#task#collect_packages(config) abort
  for l:hook in ['collect', 'config']
    for l:mp in a:config.metapack_order
      if has_key(l:mp.hooks, l:hook)
        echo 'Running '.l:hook.' on '.l:mp.name
        call call(l:mp.hooks[l:hook].function, [], l:mp)
      endif
    endfor
  endfor
  echo 'Collected '.len(a:config.pack_order).' packs from '.len(a:config.metapack_order).' metapacks'
endfunction

function! s:save_pack_keys(pack, keys, failed) abort
  if !a:failed
    for l:key in a:keys
      if has_key(a:pack, l:key)
        let a:pack.repo_pack[l:key] = a:pack[l:key]
      endif
    endfor
  endif
endfunction
function! s:pack_update_state(pack, failed) abort
endfunction
function! metapack#task#compute_sync(config) abort
  let l:sync_group = a:config.task_groups.sync

  let l:update = []
  if has_key(a:config.apply_spec, 'update')
    let l:update = a:config.apply_spec.update
    if l:update == []
      let l:update = map(copy(a:config.pack_order), {_,v->v.name})
    endif
  endif

  for l:pack in a:config.pack_order
    call l:pack.generate_work(index(l:update, l:pack.name) != -1, l:sync_group)
  endfor
  " TODO: update_spec clean option with 'gc', 'rm', etc. to git-gc or rm as needed
endfunction

let s:demand_keys = [ 'filetypes', 'commands', 'functions', 'maps' ]
function! s:is_inline(hook) abort
  return index(a:hook.flags, 'inline') != -1
endfunction

function! metapack#task#generate_load(config) abort
  let l:lines =
        \ metapack#fragment#load_order(a:config) +
        \ metapack#fragment#load_demand_data(a:config) +
        \ metapack#fragment#load_util() +
        \ metapack#fragment#load_demand_setup(a:config) +
        \ metapack#fragment#load_adjust_rtp(a:config) +
        \ metapack#fragment#load_metapacks(a:config)

  let l:file = a:config.load_file
  let l:temp = l:file . '.new'
  call writefile(l:lines, l:temp)
  call metapack#util#checked_system('mv -f ' . fnameescape(l:temp) . ' ' . fnameescape(l:file),
        \ 'Writing load.vim')
endfunction

function! metapack#task#update_remote(config) abort
  let l:saved_rtp = &rtp
  try
    let l:dirs = extend(split(&rtp, ','), map(copy(a:config.pack_order), {_,v->v.path}))
    let &rtp = join(uniq(sort(l:dirs)), ',')
    UpdateRemotePlugins
  finally
    let &rtp = l:saved_rtp
  endtry
endfunction

function! s:docfiles(path) abort
  let l:ret = []
  for l:pat in [ '/doc/*.??x', '/doc/*.txt' ]
    let l:ret += glob(a:path.l:pat, v:false, v:true)
  endfor
  return l:ret
endfunction
function! metapack#task#update_help(config) abort
  let l:doc_dir = a:config.runtime_dir.'/doc'

  let l:oldnames = map(s:docfiles(a:config.runtime_dir), {_,v->fnamemodify(v, ':t')})
  let l:newfiles = []
  for l:pack in a:config.pack_order
    let l:newfiles += s:docfiles(l:pack.path)
  endfor
  let l:newnames = map(copy(l:newfiles), {_,v->fnamemodify(v, ':t')})

  let l:count = len(l:newnames)
  let l:newnames = uniq(l:newnames)
  if len(l:newnames) != l:count
    echoerr 'Duplicate help filenames detected!'
    " We can't do much about it, so carry on.
  endif

  let l:remove = []
  for l:name in l:oldnames
    if index(l:newnames, l:name) == -1
      let l:remove += [shellescape(l:doc_dir.'/'.l:name)]
    endif
  endfor
  if len(l:remove)
    call metapack#util#checked_system('rm '.join(l:remove), 'Removing doc/ links')
  endif

  let l:create = []
  for l:file in l:newfiles
    if index(l:oldnames, fnamemodify(l:file, ':t')) == -1
      let l:create += [shellescape(l:file)]
    endif
  endfor
  if len(l:create)
    call metapack#util#checked_system('ln -s '.join(l:create).' '.shellescape(l:doc_dir),
          \ 'Creating doc/ links')
  endif

  " FIXME: don't echo
  silent! execute 'helptags' a:config.runtime_dir.'/doc'
endfunction

function! metapack#task#bootstrap(config) abort
  for l:dir in ['pack', 'metapack', 'load']
    call mkdir(a:config[l:dir.'_dir'], 'p')
  endfor
  call mkdir(a:config.runtime_dir.'/doc', 'p')

  let l:mpf = a:config.metapack_dir.'/metapack.vim'
  if glob(l:mpf) == ''
    call writefile(metapack#fragment#metapack_script(),
          \ l:mpf)
    echo 'A default metapack.vim has been created for you at'
    echo '  '.l:mpf
    echo ' '
    echo 'It is yours to keep, free of charge.  Feel free to edit it, perhaps'
    echo 'to change the MetapackApply or MetapackUpdate defaults.  Also,'
    echo 'notice that Metapack itself is actually demand-loaded only when a new'
    echo 'configuration will be applied.'
    echo ' '
  endif
  let l:lf = a:config.list_file
  if glob(l:lf) == ''
    call writefile(['Metapack metapack'], l:lf)
    echo 'A default list.vim has been created for you at'
    echo '  '.l:lf
    echo ' '
    echo 'Right now, it loads only metapack itself, but you can add more'
    echo 'metapacks to it.'
    echo ' '
  endif

  let l:snippet = 'source '.fnameescape(a:config.load_file)
  let @m = l:snippet."\n"
  echo 'The following line has been copied to "m:'
  echo '  '.l:snippet
  echo ' '
  echo 'Once Metapack finishes bootstrapping, add it to .vimrc or init.vim and'
  echo 'restart to enable Metapack permanently.'
endfunction
