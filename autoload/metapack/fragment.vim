" metapack/fragment.vim: code snippets and generation
" vim:fdm=marker:ts=2:sts=2:sw=2:et:tw=100

let s:fragments = {}
function! s:load_fragments(fn) abort
  let l:lines = readfile(a:fn)

  let l:line = 0
  while v:true
    let l:line = match(l:lines, '^Fragment\s\+\w\+$', l:line)
    if l:line == -1
      break
    endif

    let l:end = match(l:lines, '^EndFragment$', l:line + 1)
    if l:end == -1
      echoerr 'Unterminated fragment beginning at line '.l:line.'!'
      break
    endif

    let l:name = matchstr(l:lines[l:line], '\s\+\zs.*')
    let s:fragments[l:name] = metapack#hooks#strip_indent(l:lines[l:line + 1 : l:end - 1])

    let l:line = l:end + 1
  endwhile
endfunction
call s:load_fragments(expand('<sfile>:h').'/fragment-hooks.vim')

" metapack.vim for bootstrapping
function! metapack#fragment#metapack_script() abort
  return s:fragments.bootstrap_metapack
endfunction

" load.vim header
function! metapack#fragment#load_order(config) abort
  return [
        \ 'let s:metapack_order = ' . string(map(copy(a:config.metapack_order), {_,v->v.name})),
        \ 'let s:pack_order = ' . string(map(copy(a:config.pack_order), {_,v->v.name}))
        \ ]
endfunction

" load.vim utilities
function! metapack#fragment#load_util() abort
  return s:fragments.load_util
endfunction

let s:demand_keys = ['filenames', 'filetypes', 'commands', 'functions', 'maps']
function! s:is_demand(pack)
  let l:spec = a:pack.spec
  if has_key(l:spec, 'manual') && l:spec.manual
    return v:true
  endif
  for l:key in s:demand_keys
    if has_key(l:spec, l:key)
      return v:true
    endif
  endfor
  return v:false
endfunction

" load.vim demand-loading data
" FIXME: also after/; syntax/?
" FIXME: doautocmd filetype{plugin,indent}
function! metapack#fragment#load_demand_data(config) abort
  let l:lines = ['let s:pack_ondemand = {']
  for l:pack in a:config.pack_order
    let l:spec = l:pack.spec

    if s:is_demand(l:pack)
      let l:dict = {}
      if has_key(l:spec, 'commands')
        let l:dict.commands = map(copy(l:spec.commands), {_,v->matchstr(v, '^[^|!]*')})
      endif
      if has_key(l:spec, 'maps')
        let l:dict.maps = l:spec.maps
      endif

      let l:plen = len(l:pack.path) + 1
      let l:plug = map(glob(l:pack.path.'/plugin/**/*.vim', v:false, v:true), {_,v->v[l:plen:-1]})
      let l:ft = map(glob(l:pack.path.'/ftdetect/*.vim', v:false, v:true), {_,v->v[l:plen:-1]})

      let l:dict.path = l:pack.path
      if !empty(l:plug)
        let l:dict.plugin = l:plug
      endif
      if !empty(l:ft)
        let l:dict.ftdetect = l:ft
      endif

      let l:lines += ['  \'''.l:pack.name.''': '.string(l:dict).',']
    else
      echo 'Pack '.l:pack.name.' will not be demand-loaded'
    endif
  endfor
  let l:lines += ['  \}']

  return l:lines
endfunction

function! s:get_copy(obj, key, def) abort
  return has_key(a:obj, a:key) ? copy(a:obj[a:key]) : a:def
endfunction
function! s:demand_filenames(name, fns) abort
  " BufNew might be all it takes, otherwise BufNewFile and/or BufReadPre might be needed?
  return ['autocmd BufNewFile,BufRead '.a:fns.' nested call s:demand_load('''.a:name.''')']
endfunction
" FIXME: collect filetypes and demand-load in a single autocmd
function! s:demand_filetypes(name, fts) abort
  " We might need to kick filetype and/or the plugin to wake them up.
  return ['autocmd FileType '.a:fts.' nested call s:demand_load('''.a:name.''')']
endfunction
function! s:demand_command(name, command) abort
  " Create a fake command that will demand-load the plugin and forward the original call to it.  Tab
  " completion also demand-loads the plugin and retries the completion.  For now, we also interpret
  " '|' and '!' suffixes to add -bar and -bang modifiers to the command so it behaves more similarly
  " to the real command.
  "
  " TODO: it would be nice to support wildcards here, but then it takes a bunch of tabbing to get an
  " autocomplete to fire to actually load the function; also, backspace would be needed.
  let [l:name, l:mods] = matchlist(a:command, '[^!]*\ze\([!]*\)')[0:1]
  let l:bang = (stridx(l:mods, '!') != -1) ? '-bang ' : ''
  " XXX right now, we snarf up the whole command and execute it after loading.  That means it's
  " executed from load.vim, not the commandline.  Do we care about load.vim's scope?
  return 'command! -nargs=* '.l:bang.' -complete=custom,s:demand_completion '.l:name
        \ ." call s:demand_command('".a:name."', '".l:name."', "
        \ .(l:bang?'<bang>0':'0').', <q-args>)'
endfunction
function! s:demand_functions(name, functions) abort
  " NB: wildcards are supported
  return ['autocmd FuncUndefined '.a:functions.' nested call s:demand_load('''.a:name.''')']
endfunction
function! s:demand_map(name, map, mode) abort
  " FIXME: I'm pretty sure this doesn't work the way I want it to; see vim-plug's lod_map.
  " NB: wildcards are not supported, but partial mappings are: only the '<Plug>(easymotion-' in
  " '<Plug>(easymotion-n)' is needed.
  let l:emit = escape(a:map, '\<"')
  return a:mode.'map <expr> '.a:map.' <SID>demand_map('''.a:name.''', "'.l:emit.'")'
endfunction
" load.vim unrolled demand-loading infrastructure
" TODO: also, load groups of packs together.
" TODO: also, demand-load custom hooks?
function! metapack#fragment#load_demand_setup(config) abort
  let l:lines = []
  let l:auend = v:false
  for l:pack in a:config.pack_order
    let l:spec = l:pack.spec
    let l:name = l:spec.name

    let l:has_au = max(map(['filenames', 'filetypes', 'functions'], {_,v->has_key(l:spec, v)}))
    if l:has_au
      let l:lines += ['augroup Metapack/pack/'.l:name]
      let l:auend = v:true
    else
      let l:lines += ['" '.l:name]
    endif

    let l:lines += map(s:get_copy(l:spec, 'commands', []),
          \ {_,v->s:demand_command(l:name, v)})
    for [l:map, l:modes] in items(get(l:spec, 'maps', {}))
      let l:lines += map(split(l:modes, '\zs'),
            \{_,v->s:demand_map(l:name, l:map, v)})
    endfor
    if l:has_au
      for [l:key, l:Func] in items({
            \ 'filenames': function('s:demand_filenames'),
            \ 'filetypes': function('s:demand_filetypes'),
            \ 'functions': function('s:demand_functions'),
            \})
        if has_key(l:spec, l:key)
          let l:lines += l:Func(l:name, join(l:spec[l:key], ','))
        endif
      endfor
    endif
  endfor
  if l:auend
    let l:lines += ['augroup END']
  endif
  return l:lines
endfunction

" load.vim rtp adjustment
function! metapack#fragment#load_adjust_rtp(config) abort
  let l:rtp = []
  for l:mp in a:config.metapack_order
    "let l:rtp += map(copy(l:mp.pack_order), {_,v->v.path})
    for l:pack in l:mp.pack_order
      if !s:is_demand(l:pack)
        let l:rtp += [l:pack.path]
      endif
    endfor
  endfor
  " Supply our own runtime dir to ensure that help is always available
  let l:rtp += [a:config.runtime_dir]
  return ['let &rtp = '''.join(l:rtp, ',').',''.&rtp']
endfunction

" load.vim execute metapack hooks
" TODO: pack= (also metapack=?) hook flags to conditionally add?
function! metapack#fragment#load_metapacks(config) abort
  let l:inlines = []
  for l:mp in a:config.metapack_order
    if !has_key(l:mp.hooks, 'generate') && !has_key(l:mp.hooks, 'init')
      continue
    endif

    let l:extlines = []
    let l:sourced = v:false
    let l:inlines += ['augroup Metapack/meta/'.l:mp.name, 'autocmd!']

    " generate hook is guaranteed to run before init, regardless of inlining
    for l:key in ['generate', 'init']
      if !has_key(l:mp.hooks, l:key)
        continue
      endif

      let l:hook = l:mp.hooks[l:key]

      let l:inline = index(l:hook.flags, 'inline') != -1
      if !l:inline && !l:sourced
        let l:inlines += ['source '.a:config.load_dir.'/'.l:mp.name.'.vim']
        let l:sourced = v:true
      endif

      let l:target = l:inline ? l:inlines : l:extlines
      call extend(l:target, metapack#hooks#to_lines(l:hook, l:mp))
    endfor

    if l:sourced
      call writefile(l:extlines, a:config.load_dir.'/'.l:mp.name.'.vim')
    else
      " TODO remove stale files
    endif
  endfor

  let l:inlines += ['augroup END']
  return l:inlines
endfunction
