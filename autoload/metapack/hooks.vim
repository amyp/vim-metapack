" metapack/hooks.vim: hacky hook functions
" vim:fdm=marker:ts=2:sts=2:sw=2:et:tw=100
"
" metapack uses scripts which hook into its configuration generator.  hooks.vim is responsible for
" loading these hooks.  The implementation doesn't actually parse the scripts, so some tricks are
" used instead.  MetapackHook swallows the following function command (and the rest of the line by
" accident) and attaches it to an object.
"
" TODO: add an extra flavor of 'custom' for capturing a single freestanding function

function! s:abort(file, line, message) abort
  echoerr a:message.' at '.a:file.' line '.a:line.'; aborting'
  let s:hooks_failed = v:true
  return 'finish'
endfunction

function! s:gen_hook(file, line, str) abort
  if exists('s:string_hook')
    call s:finish_string()
  endif

  " FIXME: varargs functions?
  let l:sig = matchlist(a:str,
        \ '^\([[:lower:][:space:]]*\)|' .
        \ '\s*function!\?\s\+\(\w\+\)' .
        \ '\s*(\([[:alnum:][:space:],]*\))' .
        \ '\s*\(|.*\|\)$')
  if l:sig == []
    return s:abort(a:file, a:line, 'Invalid MetapackHook signature')
  endif

  let [l:flags, l:name, l:args, l:body] = l:sig[1:4]
  let l:flags = filter(split(l:flags, '\s\+'), {_,v->v != ''})
  let l:custom = index(l:flags, 'custom') != -1

  " Find the first matching pattern or any exact match
  let l:spec = {}
  for [l:key, l:val] in reverse(items(s:spec))
    if l:key == l:name
      let l:spec = l:val
      break
    elseif match(l:name, l:key) != -1
      let l:spec = l:val
    endif
  endfor

  if empty(l:spec) && !l:custom
    return s:abort(a:file, a:line, 'No such hook '.l:name)
  endif
  if l:custom
    if !empty(l:spec)
      return s:abort(a:file, a:line, 'Custom hook '.l:name.' aliases a real hook')
    endif
    let l:spec = { 'type': 'lines', 'nargs': 0 }
  endif
  if has_key(g:metapack_hook_target, l:name)
    return s:abort(a:file, a:line, 'Hook '.l:name.' already defined')
  endif

  " FIXME: check flags against a list
  let g:metapack_hook_target[l:name] = {
        \ 'flags': l:flags,
        \ 'type': l:spec.type,
        \ }

  let l:arglist = len(l:args) ? split(l:args, ',', v:true) : []
  if match(l:arglist, '^\s*$') != -1
    return s:abort(a:file, a:line, 'Empty argument name')
  endif

  if has_key(l:spec, 'nargs') && len(l:arglist) != l:spec.nargs
    return s:abort(a:file, a:line, l:spec.nargs.' arguments required for hook '.l:name)
  endif

  let l:body = substitute(substitute(l:body, '|', '\n  ', ''),
        \ '|\s*endfunction', '\nendfunction', '')

  if l:spec.type ==# 'function'
    return 'function g:metapack_hook_target.'.l:name.'.function('.join(l:arglist, ', ').')'.l:body
  elseif l:spec.type ==# 'lines'
    " Let vim parse the function, then read it back out in s:finish_string()
    let s:string_hook = l:name
    return 'function g:metapack_hook_target.string_fn('.join(l:arglist, ', ').')'.l:body
  else
    return 'function! s:ignore('.join(l:arglist, ', ').')'.l:body
  endif
endfunction

function! metapack#hooks#strip_indent(text) abort
  let l:text = copy(a:text)
  " Surely, there's a more elegant way of trimming common leading whitespace
  if len(l:text)
    let l:ws = matchstr(l:text[0], '^\s*')
    for l:idx in range(1, len(l:text) - 1)
      for l:char in range(min([len(l:ws), len(l:text[l:idx])]))
        if l:ws[l:char] != l:text[l:idx][l:char]
          let l:ws = l:char ? l:ws[0:l:char - 1] : ''
          break
        endif
      endfor
      if l:ws == ''
        break
      endif
    endfor
    if l:ws != ''
      let l:trim = len(l:ws)
      let l:text = map(l:text, {_,v->v[l:trim:-1]})
    endif
  endif

  return l:text
endfunction

function! s:finish_string() abort
  " FIXME: don't echo
  let g:metapack_hook_target[s:string_hook].lines = metapack#hooks#strip_indent(
        \ map(
        \   filter(
        \     split(
        \       execute('verbose function g:metapack_hook_target.string_fn'),
        \       "\n"),
        \     {_,v->match(v, '^\d') == 0}),
        \   {_,v->substitute(v, '^...\d*', '', '')}))

  unlet g:metapack_hook_target['string_fn']
  unlet s:string_hook
endfunction

" FIXME: need a second 'MetapackFragment' flavor?  Will nesting even work?
function! metapack#hooks#begin(spec) abort
  let s:spec = a:spec
  command! -nargs=+ MetapackHook
        \ execute s:gen_hook(expand('<sfile>:t'), expand('<slnum>'), <q-args>)
endfunction
function! metapack#hooks#end() abort
  delcommand MetapackHook
  unlet s:spec
endfunction

function! metapack#hooks#read_file(object, filename) abort
  let g:metapack_hook_target = a:object
  let s:source_file = a:filename
  let s:hooks_failed = v:false

  try
    execute 'source' a:filename
    if exists('s:string_hook')
      call s:finish_string()
    endif
  catch
    echoerr v:exception
    let s:hooks_failed = v:true
  endtry

  let l:failed = s:hooks_failed
  unlet! g:metapack_hook_target s:source_file s:hooks_failed s:source_lines
  return l:failed
endfunction

function! metapack#hooks#to_lines(hook, obj, ...) abort
  if a:hook.type ==# 'function'
    return call(a:hook.function, a:000, a:obj)
  elseif a:hook.type ==# 'lines'
    return a:hook.lines
  else
    return []
  endif
endfunction
