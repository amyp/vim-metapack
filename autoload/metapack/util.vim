function! metapack#util#as_list(arg)
  if type(a:arg) == v:t_list
    return a:arg
  else
    return [a:arg]
  endif
endfunction

function! metapack#util#checked_system(cmd, action)
  let l:stderr = system(a:cmd . ' 2>&1 >/dev/null')
  if v:shell_error
    throw a:action . ' failed: ' . l:stderr
  endif
endfunction
