" metapack/async.vim: a hacky async-capable task dispatcher
" vim:fdm=marker:ts=2:sts=2:sw=2:et:tw=100
"
" metapack/async.vim is a rudimentary serial/parallel task dispatcher.  Tasks may be synchronous
" VimL function calls or asynchronous command executions.  To control execution, tasks are grouped
" together.  Groups may be nested.  New tasks and groups may be added to existing groups, even after
" they have completed; if necessary, async.vim will backtrack to run them.  However, once all tasks
" have completed, existing groups are invalidated and cannot be reused.
"
" TODO: don't :redir.  :AsyncLog or similar, redir only on fail.
" TODO: stdout processing
" TODO: hide empty groups?
" TODO: custom status messages?  Implies async timer running, probably retained mode.
" TODO: retained mode updates are now possible.  Cache line numbers and do this.
" TODO: nested nvim instance for async rpc calls?

" XXX remove
if exists('s:work')
  unlet s:work
endif

let s:bufnr = get(s:, 'bufnr', -1)
let s:bufvis = get(s:, 'bufvis', v:false)
let s:avail_jobs = get(s:, 'avail_jobs', get(g:, 'metapack_jobs', 8))

let s:NEW = -1 " Task has not been displayed yet (shown_status only)
let s:WAITING = 0 " Task has not executed yet
let s:RUNNING = 1 " Task is running asynchronously
let s:FINISHED = 2 " Task completed successfully
let s:FAILED = 3 " Task failed to complete
let s:ABORTED = 4 " A previous failure prevented this task from running
let s:status = [ 'waiting', 'running', 'finished', 'failed', 'aborted' ]
lockvar s:NEW s:WAITING s:RUNNING s:FINISHED s:FAILED s:ABORTED s:status

"{{{ Buffer management
function! s:init_buffer() abort
  if s:bufnr != -1
    if !s:bufvis
      execute 'sbuffer' s:bufnr
    endif
    return
  endif

  new
  setlocal buftype=nofile bufhidden=hide noswapfile nomodifiable nobuflisted
  file Metapack\ log
  let s:bufnr = bufnr('%')
  let s:bufvis = v:true

  augroup Metapack
    autocmd!
    autocmd BufUnload <buffer> let s:bufnr = -1|let s:bufvis = v:false
    autocmd BufHidden,TabLeave <buffer> let s:bufvis = v:false
    autocmd BufWinEnter,TabEnter <buffer> let s:bufvis = v:true|call s:update_buffer(v:false)
  augroup END
endfunction

function! s:update_buffer(redraw) abort
  if !s:bufvis || !exists('s:work')
    return
  endif

  let l:cur_win = winnr()
  let l:buf_win = bufwinnr(s:bufnr)
  if l:buf_win != -1
    execute l:buf_win.'wincmd w'
    setlocal modifiable

    " Scan tasks and redraw as needed
    let l:ret = 1
    for l:child in s:work.children
      let l:ret = s:recursive_display_work(l:child, l:ret, 0)
    endfor

    " Clean up after a stale buffer
    if l:ret <= line('$')
      silent execute l:ret . ',$delete _'
    endif

    setlocal nomodifiable
    execute l:cur_win.'wincmd w'

    if a:redraw
      redraw
    endif
  endif
endfunction
"}}}
"{{{ Group & task constructors
function! s:add_child(parent, child) abort
  let a:parent.children += [a:child]
  let a:child.parent = a:parent

  call s:restart_parents(a:child)

  return a:child
endfunction

let s:task_proto = { 'status': s:WAITING, 'lines': 1, 'shown_status': s:NEW }
function s:task_proto.status_name()
  return s:status[self.status]
endfunction
function s:task_proto.status_message()
  return (has_key(self, 'spec') && type(self.spec) == v:t_dict && has_key(self.spec, 'status'))
        \ ? self.spec.status(self) : self.status_name()
endfunction

let s:group_proto = copy(s:task_proto)
function s:group_proto.serial_group(name) dict abort
  return s:add_child(self, s:serial(a:name))
endfunction
function s:group_proto.parallel_group(name) dict abort
  return s:add_child(self, s:parallel(a:name))
endfunction
function s:group_proto.task(name, spec) dict abort
  return s:add_child(self, s:task(a:name, a:spec))
endfunction
function s:group_proto.call(name, Func, ...) dict abort
  return s:add_child(self, s:task(a:name, {'call': a:Func, 'call_args': a:000}))
endfunction
function s:group_proto.cmd(name, cmd) dict abort
  return s:add_child(self, s:task(a:name, {'command': a:cmd}))
endfunction

function! s:serial(name) abort
  let l:ret = copy(s:group_proto)
  let l:ret.name = a:name
  let l:ret.children = []
  let l:ret.serial = v:true
  return l:ret
endfunction
function! s:parallel(name) abort
  let l:ret = copy(s:group_proto)
  let l:ret.name = a:name
  let l:ret.children = []
  let l:ret.serial = v:false
  return l:ret
endfunction

function! s:add_message(work, message) abort
  if len(a:message) == 0
    return
  endif

  if !has_key(a:work, 'message')
    let a:work.message = []
  endif
  if a:message[-1] == ''
    let a:work.message += a:message[0:-2]
  else
    let a:work.message += a:message
  endif
endfunction

function! s:task_call(work, name, args) abort
  let l:rc = v:false
  if has_key(a:work.spec, a:name)
    try
      redir => l:msg
      silent! let l:rc = call(a:work.spec[a:name], a:args)
      redir END
    catch
      redir END
      let l:rc = v:true
      let l:msg .= v:exception."\n"
    endtry
    call s:add_message(a:work, split(l:msg, '\n'))
  endif
  return l:rc
endfunction
function! s:task_finish(work, failed) abort
  let l:args = get(a:work.spec, 'post_args', []) + [a:failed]
  let l:rc = s:task_call(a:work, 'post', l:args) || a:failed
  return l:rc ? s:FAILED : s:FINISHED
endfunction

function! s:async_exit(id, data, event) dict abort
  let l:work = self.work
  call s:add_message(l:work, self.stderr)
  let l:work.status = s:task_finish(l:work, a:data)

  let s:avail_jobs += 1
  call s:handle_work()
endfunction

function! s:task_proto.spawn() dict abort
  if type(self.spec) == v:t_func
    let self.spec = self.spec()
  endif

  let l:failed = s:task_call(self, 'call', get(self.spec, 'call_args', []))

  if !l:failed && has_key(self.spec, 'command')
    let l:opts = extend(copy(get(self.spec, 'command_opts', {})), {
          \ 'stderr_buffered': v:true,
          \ 'on_exit': function('s:async_exit'),
          \ 'work': self,
          \ })
    let l:id = jobstart(self.spec.command, l:opts)
    return l:id == -1 ? s:FAILED : s:RUNNING
  endif

  return s:task_finish(self, l:failed)
endfunction

function! s:task(name, spec) abort
  let l:ret = copy(s:task_proto)
  let l:ret.name = a:name
  let l:ret.spec = a:spec
  return l:ret
endfunction
"}}}
"{{{ Task display
function! s:display_work(work, line, indent) abort
  if a:work.shown_status != a:work.status
    let l:msg = repeat(' ', a:indent) . a:work.name . ': ' . a:work.status_message()

    if a:work.shown_status == s:NEW
      call append(a:line - 1, l:msg)
    else
      call setline(a:line, l:msg)
    endif
    let a:work.shown_status = a:work.status

    if a:work.status >= s:FINISHED && has_key(a:work, 'message') && len(a:work.message) > 0
      let l:msg = a:work.message
      if len(l:msg) > 0
        let l:pfx = repeat(' ', a:indent + 2)
        let a:work.lines = 1 + len(l:msg)
        call append(a:line, map(l:msg, {_,v->substitute(l:pfx . v, '^\s\+$', '', '')}))
        unlet a:work.message
      endif
    endif
  endif
  return a:line + a:work.lines
endfunction

function! s:recursive_display_work(work, line, indent) abort
  let l:ret = a:line
  if has_key(a:work, 'name')
    let l:ret = s:display_work(a:work, l:ret, a:indent)
  endif
  if has_key(a:work, 'children')
    for l:child in a:work.children
      let l:ret = s:recursive_display_work(l:child, l:ret, a:indent + 2)
    endfor
  endif
  return l:ret
endfunction
"}}}
"{{{ Task execution
function! s:restart_parents(work) abort
  if a:work.status < s:FINISHED
    " Nothing
  elseif a:work.status == s:FINISHED
    let a:work.status = s:WAITING
  elseif has_key(a:work, 'serial') && a:work.serial
    return s:ABORTED
  else
    let a:work.status = s:WAITING
  endif

  if has_key(a:work, 'parent')
    let l:status = s:restart_parents(a:work.parent)
    if l:status == s:ABORTED
      let a:work.status = s:ABORTED
      return s:ABORTED
    endif
  endif
  return s:WAITING
endfunction

function! s:abort_work(work) abort
  let a:work.status = s:ABORTED

  if has_key(a:work, 'children')
    for l:child in a:work.children
      call s:abort_work(l:child)
    endfor
  endif
  if has_key(a:work, 'spec')
    call s:task_finish(a:work, v:true)
  endif
endfunction

function! s:recursive_handle_work(work) abort
  if a:work.status < s:FINISHED
    if has_key(a:work, 'spawn')
      if a:work.status > s:WAITING
        return a:work.status
      endif
      let a:work.status = a:work.spawn()
      if a:work.status == s:RUNNING
        let s:avail_jobs -= 1
      endif
    elseif has_key(a:work, 'children')
      let l:failed = v:false
      let l:running = v:false
      for l:child in a:work.children
        if l:failed && a:work.serial
          call s:abort_work(l:child)
          continue
        endif

        let l:status = s:avail_jobs ? s:recursive_handle_work(l:child) : s:WAITING

        if a:work.serial
          " Serial groups: failure should abort subsequent tasks except in root group;
          " running/waiting tasks block subsequent tasks.
          if l:status == s:FAILED && a:work.name != v:null
            let l:failed = v:true
          endif
          if l:status <= s:RUNNING
            let l:running = v:true
            break
          endif
        else
          " Parallel groups: failure should be indicated after all tasks have completed; any number
          " of tasks may be started.
          if l:status == s:FAILED
            let l:failed = v:true
          elseif l:status <= s:RUNNING
            let l:running = v:true
            if l:status == s:WAITING
              break
            endif
          endif
        endif
      endfor

      let a:work.status = l:running ? s:RUNNING : l:failed ? s:FAILED : s:FINISHED
    endif
  endif

  return a:work.status
endfunction

function! s:handle_work() abort
  call s:recursive_handle_work(s:work)
  call s:update_buffer(v:false)
  if s:work.status >= s:FINISHED
    unlet s:work
  endif
endfunction
"}}}
"{{{ API entry point
function! metapack#async#async() abort
  if !exists('s:work')
    let s:work = s:serial(v:null)
  endif
  function! s:work.run() dict
    call s:update_buffer(v:true)
    call s:handle_work()
  endfunction
  call s:init_buffer()
  return s:work
endfunction
"}}}
