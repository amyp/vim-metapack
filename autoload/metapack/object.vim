function! s:check_list(list, Pred) abort
  for l:i in a:list
    if a:Pred(l:i)
      return v:true
    endif
  endfor
  return v:false
endfunction

"{{{ config: new configuration being generated
let s:config_proto = {
      \ 'user_config': {},
      \ 'apply_spec': {},
      \ 'task_groups': {},
      \ 'metapacks': {},
      \ 'metapack_order': [],
      \ 'packs': {},
      \ 'pack_order': [],
      \ }

function! s:config_proto.add_metapack(name) dict abort
  let l:mp = s:metapack(self, a:name)
  if has_key(self.metapacks, l:mp.name)
    throw 'Metapack '.l:mp.name.' already exists'
  endif

  let self.metapacks[l:mp.name] = l:mp
  let self.metapack_order += [l:mp]
  return l:mp
endfunction
function! s:config_proto.add_metapacks(list) dict abort
  for l:name in a:list
    call self.add_metapack(l:name)
  endfor
endfunction

function! s:config_proto.add_pack(name, opts) dict abort
  let l:pack = s:pack_spec(self, a:name, a:opts)
  if has_key(self.packs, l:pack.name)
    throw 'Package '.l:pack.name.' already exists'
  endif

  let l:req = s:pack_request(self, l:pack)
  let self.packs[l:pack.name] = l:req
  let self.pack_order += [l:req]
  return l:req
endfunction

" Friendly interface
function! s:config_proto.has_metapack(name) dict abort
  return s:check_list(self.metapack_order, {v->v.name == a:name})
endfunction
function! s:config_proto.has_pack(name) dict abort
  return s:check_list(self.pack_order, {v->v.name == a:name})
endfunction
" XXX should we make this available at runtime?
function! s:config_proto.get_config(name, default) dict abort
  if has_key(self.user_config, a:name)
    return self.user_config[a:name]
  else
    let self.user_config[a:name] = a:default
    return a:default
  endif
endfunction

function! metapack#object#config(root_dir) abort
  return extend(deepcopy(s:config_proto), {
        \ 'root_dir': a:root_dir,
        \ 'runtime_dir': a:root_dir.'/runtime',
        \ 'pack_dir': a:root_dir.'/pack',
        \ 'metapack_dir': a:root_dir.'/meta',
        \ 'load_dir': a:root_dir.'/load',
        \ 'load_file': a:root_dir.'/load.vim',
        \ 'list_file': a:root_dir.'/list.vim',
        \ })
endfunction
"}}}
"{{{ metapack: user-visible configuation objects
let s:metapack_proto = {
      \ 'hooks': {},
      \ 'packs': {},
      \ 'pack_order': [],
      \ }

function! s:metapack_proto.add_pack(name, ...) abort
  let l:opts = (a:0 == 1 && type(a:1) == v:t_dict) ? a:1 : {}
  if a:0 > 0 && (a:0 > 1 || type(a:1) != v:t_dict)
    throw 'Invalid arguments to add_pack'
  endif

  let l:req = self.config.add_pack(a:name, l:opts)
  let self.packs[l:req.name] = l:req
  let self.pack_order += [l:req]

  return l:req
endfunction

function! s:metapack_proto.load_packs() abort
  let l:in_mp = exists('*MetapackDemandLoad')
  for l:pack in self.pack_order
    " Once we update packages, it's no longer safe to demand-load them from the existing
    " configuration.  We need to check for and load them ourselves.
    " TODO: notify running configuration that we are stepping on its toes
    let l:path = l:pack.path
    if &rtp !~# '\(^\|,\)'.escape(l:path, '[]\/.').'\($\|,\)'
      let &rtp .= ','.l:path
      for l:script in glob(l:path.'/plugin/**/*.vim', v:false, v:true)
        execute 'source' l:script
      endfor
    endif
  endfor
endfunction

function! s:metapack(config, name) abort
  let l:name = fnamemodify(a:name, ':t')
  if l:name == ''
    throw 'Blank metapack filename not allowed'
  endif
  if l:name =~ '^\.'
    throw 'Metapack filename may not begin with "."'
  endif
  if a:config.has_metapack(l:name)
    throw 'Metapack '.l:name.' already exists'
  endif

  let l:src = a:config.metapack_dir.'/'.a:name.'.vim'
  if glob(l:src) == ''
    throw 'Cannot find configuration for metapack '.a:name
  endif

  return extend(deepcopy(s:metapack_proto), {
        \ 'name': l:name,
        \ 'path': l:src,
        \ 'config': a:config,
        \ })
endfunction
"}}}
"{{{ pack_spec: details of a user request for a package
let s:valid_pack_keys = [
      \ 'name', 'branch', 'tag', 'update',
      \ 'filenames', 'filetypes', 'commands', 'functions', 'maps', 'manual'
      \]
function! s:pack_spec(config, name, opts) abort
  for l:key in keys(a:opts)
    if index(s:valid_pack_keys, l:key) == -1
      throw 'Invalid pack option '.l:key
    endif
  endfor

  let l:name = substitute(a:name, '/*$', '', '')
  let l:short = fnamemodify(l:name, ':t')
  if a:config.has_pack(l:short)
    throw 'Pack '.l:short.' already exists'
  endif

  let l:scheme = 'github'
  let l:path = l:name
  if match(l:name, '^[a-z]\+://') != -1
    let l:scheme = matchstr(l:name, '^[a-z]*')
    let l:path = matchstr(l:name, '://\zs.*')
  elseif isdirectory(expand(l:name))
    let l:scheme = 'file'
    let l:path = expand(l:name)
  endif

  let l:url = ''
  if l:scheme ==# 'github'
    let l:url = 'https://github.com/'.l:path.'.git'
  elseif l:scheme ==# 'local'
    " Git has tricks up its sleeve for local repos.  file:// avoids them for consistency.
    let l:url = 'file://'.l:path
  elseif l:scheme !=# 'file'
    throw 'Unknown URI scheme '.l:scheme
  endif

  let l:ret = extend(copy(a:opts), {
        \ 'name': l:short,
        \ 'scheme': l:scheme,
        \ 'path': l:path,
        \ 'url': l:url,
        \ }, 'keep')

  for l:key in ['update', 'filenames', 'filetypes', 'commands', 'functions']
    if has_key(l:ret, l:key)
      let l:ret[l:key] = metapack#util#as_list(l:ret[l:key])
    endif
  endfor

  return l:ret
endfunction
"}}}
"{{{ pack_req: data required to satisfy a request for a package
let s:pack_req_proto = {
      \ }
function! s:pack_req_proto.git_commitid(name) dict abort
  return system('git -C '.shellescape(self.path).
        \ ' rev-parse --verify --quiet '.shellescape(a:name))
endfunction
function! s:pack_req_proto.check_status(named) dict abort
  if self.spec.scheme ==# 'file'
    " We want update hooks to run unconditionally, so leave status alone
    if isdirectory(self.path)
      return 'external'
    endif
  elseif isdirectory(self.path)
    return self.git_commitid('HEAD')
  endif
  return 'missing'
endfunction

function! s:call_rtp(path, Func) abort
  if &rtp !~# '\(^\|,\)'.escape(a:path, '\/').'\(,\|$\)'
    let l:save = &rtp
    let &rtp .= ','.a:path
    call a:Func()
    let &rtp = l:save
  else
    call a:Func()
  endif
endfunction

" Generate an async spec to conditionally trigger updates
function! s:update_spec(pack, Step) abort
  let l:new_status = a:pack.check_status(v:false)
  if l:new_status ==# 'external' || l:new_status !=# a:pack.status
    "return {'command': a:pack.spec.update, 'command_opts': { 'cwd': a:pack.path } }
    if type(a:Step) == v:t_func
      return {'call': function('s:call_rtp', [a:pack.path, a:Step])}
    else
      return {'command': a:Step, 'command_opts': { 'cwd': a:pack.path } }
    endif
  else
    return {}
  endif
endfunction

function! s:pack_req_proto.generate_work(update, group) dict abort
  let self.status = self.check_status(v:false)
  let l:path = shellescape(self.path)

  let l:tasks = []
  let l:might_update = v:false
  if self.spec.scheme ==# 'file'
    if !a:update
      return
    endif
    let l:might_update = v:true
  else
    let l:ref = get(self.spec, 'tag', get(self.spec, 'branch', ''))
    let l:escref = (l:ref == '') ? '' : shellescape(l:ref)
    let l:repo = shellescape(self.spec.url)

    if self.status ==# 'missing'
      " We haven't seen this repo yet.  Clone.
      let l:might_update = v:true
      let l:branch = (l:ref == '') ? '' : '-b '.l:escref.' '
      let l:opts = '--depth=1 --no-tags --single-branch'
            \ .' --shallow-submodules --recurse-submodules --jobs 2 '
      " XXX it might be better to clone the default branch unconditionally, then the branch we're
      " interested in.
      let l:tasks += [[
            \ 'git clone '.self.spec.path,
            \ 'git clone '.l:opts.l:branch.l:repo.' '.l:path
            \ ]]
    elseif a:update || (l:ref != '' && self.git_commitid(l:ref) !=# self.status)
      let l:url = substitute(system('git -C '.l:path.' remote get-url origin'), '\n', '', '')
      if l:url != self.spec.url
        call metapack#util#checked_system(
              \ 'git -C '.l:path.' remote set-url origin '.shellescape(self.spec.url),
              \ 'Updating repo url')
      endif

      " The repo isn't in the requested state.  Checkout, possibly also fetch.
      let l:might_update = v:true

      " FIXME: updates are slow and verbose.  We don't really need to display HEAD->FETCH_HEAD for
      " every fetch, and we probably don't need to checkout unless HEAD changed.  Also, we'd like to
      " fetch the branch and/or tag associated with HEAD. (this should work like branch='HEAD' with
      " explicit refspec?)
      if (a:update && !has_key(self.spec, 'tag')) || self.git_commitid(l:ref) == ''
        " Either we're updating a branch or we haven't seen this ref yet.  Fetch.
        let l:refspec = (l:ref == '') ? '' :
              \ has_key(self.spec, 'tag') ? shellescape('tag '.l:ref) : l:escref
        let l:opts = '--depth=1 --no-tags --recurse-submodules=on-demand --jobs 2 '
        let l:tasks += [[
              \ 'git fetch'.((l:ref == '') ? '' : ' '.l:ref),
              \ 'git -C '.l:path.' fetch --quiet --update-head-ok '.l:opts.l:repo.' '.l:refspec
              \ ]]
      endif
      " We have the commit we're looking for.  Checkout.
      let l:tasks += [[
            \ 'git checkout',
            \ 'git -C '.l:path.' checkout --recurse-submodules --quiet '.
            \   ((l:ref == '') ? 'FETCH_HEAD' : l:escref)
            \ ]]
    endif
  endif

  if len(l:tasks) == 0 && self.spec.scheme !=# 'file'
    return
  endif

  let l:group = a:group.serial_group('Update '.self.name)
  for [l:desc, l:op] in l:tasks
    call l:group.cmd(l:desc, l:op)
  endfor
  if has_key(self.spec, 'update') && l:might_update
    "call l:group.task(self.spec.update, function('s:update_spec', [self]))
    for l:Step in self.spec.update
      let l:str = (type(l:Step) == v:t_string) ? l:Step : 'Vim function'
      call l:group.task(l:str, function('s:update_spec', [self, l:Step]))
    endfor
  endif
endfunction

function! s:pack_request(config, pack) abort
  let l:path = a:pack.scheme == 'file' ? a:pack.path : a:config.pack_dir.'/'.a:pack.name
  return extend(deepcopy(s:pack_req_proto), {
        \ 'name': a:pack.name,
        \ 'spec': a:pack,
        \ 'config': a:config,
        \ 'path': l:path,
        \ })
endfunction
"}}}
