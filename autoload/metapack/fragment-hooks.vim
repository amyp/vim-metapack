" vim:fdm=marker:ts=2:sts=2:sw=2:et:tw=100

Fragment bootstrap_metapack
  MetapackHook|function collect()
    "call a:metapack.add_pack('decimalman/vim-metapack', {
    call self.add_pack('/home/ryan/build/vim/vim-metapack', {
          \ 'commands': [ 'MetapackBootstrap' ],
          \ 'functions': [ 'metapack#*' ],
          \ })
  endfunction

  MetapackHook inline|function generate()
    " Save the current configuration's paths as the new defaults
    return ['let g:metapack_dir = '.string(self.config.root_dir)]
  endfunction

  MetapackHook inline|function init()
    " Apply the current configuration
    command! -nargs=0 MetapackApply call metapack#apply(metapack#default_config(), {})
    " Update the named packages (or all if no arguments are given)
    command! -nargs=* MetapackUpdate
          \ call metapack#apply(metapack#default_config(), {'update': [<f-args>]})
  endfunction
EndFragment

" FIXME: I don't like the use of doautocmd here.  I'd rather metapacks define hooks: pack_xypack()
" to be called during load.vim or after demand loading.  Problem: execute() can't use the sourced
" scope for non-inline.
"
" FIXME: globbing or not, it's probably faster to load packs with :runtime than many :sources.
" OTOH, there's usually very few files to source.  Profile!
"
" FIXME: run e.g. FileType autocmds on demand-load (tpope/vim-endwise!).  What (if anything) else
" does vim-plug or :packadd do?
Fragment load_util
  function! MetapackHasMetapack(name) abort
    return index(s:metapack_order, a:name) != -1
  endfunction
  function! MetapackHasPack(name) abort
    return index(s:pack_order, a:name) != -1
  endfunction
  function! MetapackDemandLoad(...) abort
    for l:name in a:000
      if has_key(s:pack_ondemand, l:name)
        call s:demand_load(l:name)
      endif
    endfor
  endfunction

  function! s:demand_load(name) abort
    if !has_key(s:pack_ondemand, a:name)
      echoerr 'Demand-loading not possible for '.a:name
      return v:false
    endif

    let l:pack = s:pack_ondemand[a:name]
    unlet s:pack_ondemand[a:name]

"    let l:restore = ''
"    if get(g:, 'did_load_filetypes', 0)
"      let l:restore = 'filetype ' .
"            \ (get(g:, 'did_load_ftplugin', 0) ? 'plugin ' : '') .
"            \ (get(g:, 'did_indent_on', 0) ? 'indent ' : '') .
"            \ 'on|'
"      filetype plugin indent off
"      filetype off
"    endif
"    if get(g:, 'syntax_on', 0)
"      let l:restore .= 'syntax on|'
"      syntax off
"    endif
"    let l:restore .= 'echoerr "Loaded '.a:name.'"'

    " Nice for debugging, but too chatty
    "if !v:vim_did_enter
    "  " There's no reason to demand-load if it won't help startup time
    "  echoerr 'Warning: demand-loading ' . a:name . ' before VimEnter'
    "endif

    if has_key(l:pack, 'maps')
      for [l:map, l:modes] in items(l:pack.maps)
        execute substitute(l:modes, '.', '&unmap '.l:map.'|', 'g')
      endfor
    endif
    if has_key(l:pack, 'commands')
      execute 'delcommand '.join(l:pack.commands, '|delcommand ')
    endif

    silent! execute join(['autocmd!', '|augroup!', ''], ' Metapack/pack/'.a:name)

    let &rtp = l:pack.path . ',' . &rtp
    if has_key(l:pack, 'plugin')
      execute 'source '.join(map(l:pack.plugin, {_,v->l:pack.path.'/'.v}), '|source ')
    endif
    if has_key(l:pack, 'ftdetect')
      execute 'augroup filetypedetect|source '
            \ .join(map(l:pack.ftdetect, {_,v->l:pack.path.'/'.v}), '|source ')
            \ .'|augroup END'
    endif

    if exists('#Metapack/load/'.a:name)
      execute 'doautocmd <nomodeline> User Metapack/load/'.a:name
    endif

    if has_key(l:pack, 'ftdetect')
      filetype detect
    else
      " FIXME: only doauto when pack has filetypes?
      " FIXME: at the very least, don't recurse infinitely
      "execute 'doautocmd <nomodeline> FileType' &filetype
    endif

    "execute l:restore

    return v:true
  endfunction

  function! s:demand_completion(lead, cmd, pos) abort
    let l:cmd = split(a:cmd, '\W')[0]
    for [l:pack, l:dict] in items(s:pack_ondemand)
      if has_key(l:dict, 'commands') && index(l:dict.commands, l:cmd) != -1
        if s:demand_load(l:pack)
          call feedkeys(" \<BS>".nr2char(&wildchar), 'int')
        endif
        break
      endif
    endfor
    return ''
  endfunction

  function! s:demand_command(name, command, bang, args) abort
    if s:demand_load(a:name)
      execute a:command.(a:bang?'!':'') a:args
    endif
  endfunction

  function! s:demand_map(name, keys) abort
    if s:demand_load(a:name)
      call feedkeys(a:keys, 'im')
    endif
    return ''
  endfunction
EndFragment
