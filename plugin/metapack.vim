" metapack.vim: plugin configuration generator
" vim:fdm=marker:ts=2:sts=2:sw=2:et:tw=100
" FIXME: move to autoload?
" TODO: investigate online (re)load of individual metapacks.  This could work:
"  - Add optional unload hooks to metapacks
"  - Add execute() backdoors into metaplug load scripts to inject stuff into their scope
"    (g:metapack_inject, :so?)
"  - Add an execute() backdoor to load.vim to inject stuff into its scope (MetapackInject()?)
"  - Add a function to load.vim to update not-yet-loaded demand-load packs
"  - Add a spec option for temporary application:
"   - run metapack unloads where defined
"   - don't generate load scripts, inject stuff into scopes instead
"   - manually load packs if not loaded
"   - link but don't unlink help; regen tags
"   - can't really deal with remote plugins.

if exists('g:metapack_loaded')
  finish
endif
let g:metapack_loaded = v:true

function! metapack#apply(config, spec) abort
  let a:config.apply_spec = a:spec
  let l:async = metapack#async#async()

  let l:apply = l:async.serial_group('Apply configuration')

  if get(a:spec, 'bootstrap', v:false)
    call l:apply.call('Install bootstrap files',
          \ function('metapack#task#bootstrap'), a:config)
  endif

  let l:collect = l:apply.serial_group('Load configuration')
  call l:collect.call('Load metapack list',
        \ function('metapack#task#load_metapacks'), a:config)
  call l:collect.call('Collect packages',
        \ function('metapack#task#collect_packages'), a:config)

  call l:apply.call('Generate sync tasks',
        \ function('metapack#task#compute_sync'), a:config)
  let l:sync = l:apply.parallel_group('Sync repositories')

  let l:install = l:apply.serial_group('Update configuration')
  call l:install.call('Generate load.vim',
        \ function('metapack#task#generate_load'), a:config)
  call l:install.call('Update remote plugins',
        \ function('metapack#task#update_remote'), a:config)
  call l:install.call('Generate help tags',
        \ function('metapack#task#update_help'), a:config)

  let l:clean = l:apply.serial_group('Clean up')

  let a:config.task_groups = {
        \ 'collect': l:collect,
        \ 'sync': l:sync,
        \ 'apply': l:apply,
        \ 'clean': l:clean,
        \ }
  call l:async.run()
endfunction

function! metapack#default_config() abort
  return metapack#object#config(g:metapack_dir)
endfunction

function! s:bootstrap() abort
  let l:path = get(g:, 'metapack_dir', stdpath('config').'/metapack')

  echo 'Metapack will be bootstrapped using the following directory:'
  echo '  '.l:path
  echo 'If this is not acceptable, change g:metapack_dir.'
  if confirm('Proceed?', "&Yes\n&No", 1) != 1
    return
  endif

  let l:cfg = metapack#object#config(l:path)
  let l:spec = { 'bootstrap': v:true, 'metapack_list': ['metapack'] }
  call metapack#apply(l:cfg, l:spec)
endfunction

command! -nargs=0 MetapackBootstrap call s:bootstrap()
